# CustomCheckbox (module pour Omeka S)

Module pour Omeka S qui ajoute un champ CustomCheckbox à tous les items sur les vues ADD, EDIT et SHOW.

Inspiré par :
- [AltText](https://github.com/zerocrates/AltText)
- [Mapping](https://github.com/omeka-s-modules/Mapping)

## Utilisation

- les CustomCheckbox sont exposés par la Rest API :
```
api/custom_checkbox
```

- le CustomCheckbox de l'o:id voulu : 
```
api/custom_checkbox/{o:id_du_custom_checkbox}
```

- le CustomCheckbox lié à l'o:id voulu :
```
api/custom_checkbox?item_id={o:id_item}
```

- avec l'API PHP :
```
$response = $this->api()->search('custom_checkbox', ['item_id' => $item->id()]);
```

- sur l'API resource "items" : 
```
$response = $this->api()->search('items', [
    'module:customCheckbox' => 'boolean',
]);
```

```
api/items?module:customCheckbox={"boolean"}
```


## Installation

- [Voir le manuel](http://omeka.org/s/docs/user-manual/install/)
- nom du dossier : `CustomCheckbox`



## Licence

GNU Affero General Public License v3.0