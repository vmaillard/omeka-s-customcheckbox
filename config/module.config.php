<?php
namespace CustomCheckbox;

return [
    'api_adapters' => [
        'invokables' => [
            'custom_checkbox' => Api\Adapter\CustomCheckboxAdapter::class,
        ],
    ],
    'entity_manager' => [
        'mapping_classes_paths' => [
            dirname(__DIR__) . '/src/Entity',
        ],
        'proxy_paths' => [
            dirname(__DIR__) . '/data/doctrine-proxies',
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__) . '/view',
        ],
    ],
    'form_elements' => [
            'factories' => [
                'CustomCheckbox\Form\ConfigForm' => 'CustomCheckbox\Service\Form\ConfigFormFactory'
            ],
    ],
    'customCheckbox' => [
        'config' => [
            'foo' => '',
        ],
    ],
];