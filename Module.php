<?php
namespace CustomCheckbox;

use Doctrine\ORM\Events;
use CustomCheckbox\Db\Event\Listener\DetachOrphanMappings;
use Omeka\Entity\Item as ItemEntity;
use Omeka\Api\Representation\ItemRepresentation as ItemRepresentation;
use Omeka\Module\AbstractModule;
use Omeka\Permissions\Acl;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceLocatorInterface;


use Laminas\View\Model\ViewModel;
use Laminas\Mvc\Controller\AbstractController;
use Laminas\View\Renderer\PhpRenderer;
use CustomCheckbox\Form\ConfigForm;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        parent::onBootstrap($event);

        $acl = $this->getServiceLocator()->get('Omeka\Acl');
        $acl->allow(
            [Acl::ROLE_AUTHOR,
                Acl::ROLE_EDITOR,
                Acl::ROLE_GLOBAL_ADMIN,
                Acl::ROLE_REVIEWER,
                Acl::ROLE_SITE_ADMIN,
            ],
            ['CustomCheckbox\Api\Adapter\CustomCheckboxAdapter',
             'CustomCheckbox\Entity\CustomCheckbox',
            ]
        );

        $acl->allow(
            null,
            ['CustomCheckbox\Api\Adapter\CustomCheckboxAdapter',
                'CustomCheckbox\Entity\CustomCheckbox',
            ],
            ['show', 'browse', 'read', 'search', 'create', 'update', 'delete']
            );

        $em = $this->getServiceLocator()->get('Omeka\EntityManager');
        $em->getEventManager()->addEventListener(
            Events::preFlush,
            new DetachOrphanMappings
        );
    }

    // `php application/data/scripts/update-db-data-module.php CustomCheckbox` pour mettre à jour le data model 
    public function install(ServiceLocatorInterface $serviceLocator)
    {
        $conn = $serviceLocator->get('Omeka\Connection');
        $conn->exec('CREATE TABLE custom_checkbox (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, custom_checkbox LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_FBA9EF89126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $conn->exec('ALTER TABLE custom_checkbox ADD CONSTRAINT FK_FBA9EF89126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
    }
    
    public function uninstall(ServiceLocatorInterface $serviceLocator)
    {
        $conn = $serviceLocator->get('Omeka\Connection');
        $conn->exec('DROP TABLE IF EXISTS custom_checkbox');
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        // Affiche CustomCheckbox sur la vue item SHOW
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.show.after',
            [$this, 'handleViewShowAfter']
        );

        // Affiche CustomCheckbox sur les vues item ADD et EDIT
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.add.form.after',
            [$this, 'handleViewFormAfter']
        );
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.edit.form.after',
            [$this, 'handleViewFormAfter']
        );

        // Add the "custom_checkbox" filter to item search.
        $sharedEventManager->attach(
            'Omeka\Api\Adapter\ItemAdapter',
            'api.search.query',
            [$this, 'handleApiSearchQuery']
        );

        // Ajoute CustomCheckbox à l'item JSON-LD
        $sharedEventManager->attach(
            'Omeka\Api\Representation\ItemRepresentation',
            'rep.resource.json',
            [$this, 'filterItemJsonLd']
        );

        // Hydrate CustomCheckbox pour API POST 
        $sharedEventManager->attach(
            'Omeka\Api\Adapter\ItemAdapter',
            'api.hydrate.post',
            [$this, 'hydrateCustomCheckbox']
        );
    }

    /*
        Affiche CustomCheckbox sur la vue item SHOW
    */
    public function handleViewShowAfter(Event $event)
    {
        echo $event->getTarget()->partial('common/custom-checkbox-view');
    }

    /*
        Affiche CustomCheckbox sur les vues items ADD et EDIT
    */
    public function handleViewFormAfter(Event $event)
    {
        echo $event->getTarget()->partial('common/custom-checkbox-form');
    }

    // Add the "custom_checkbox" / "module:customCheckbox" filter to item search.
    public function handleApiSearchQuery(Event $event)
    {
        $query = $event->getParam('request')->getContent();
        if (isset($query['module:customCheckbox'])) {
            $qb = $event->getParam('queryBuilder');
            $itemAdapter = $event->getTarget();
            $mappingMarkerAlias = $itemAdapter->createAlias();

            $qb->innerJoin(
                'CustomCheckbox\Entity\CustomCheckbox', 
                'cf', 
                'WITH', 
                'omeka_root' . '.id = cf.item')
                ->where('cf.customCheckbox = :custom_checkbox')
                ->setParameter('custom_checkbox', $query['module:customCheckbox']);
        }
    }

    /*
        Ajoute CustomCheckbox à l'item JSON-LD  
    */
    public function filterItemJsonLd(Event $event)
    {   
        $customCheckboxForJsonLd = null;
        $item = $event->getTarget();
        $jsonLd = $event->getParam('jsonLd');
        $api = $this->getServiceLocator()->get('Omeka\ApiManager');

        $customCheckbox = $this->getCustomCheckboxForItem($item);
        if ($customCheckbox) {
            $customCheckboxForJsonLd = $customCheckbox->getCustomCheckbox();
        }
        // recup de params custom
        // $globalSettings = $this->getServiceLocator()->get('Omeka\Settings');
        // $custom_checkbox_setting = $globalSettings->get('custom_checkbox_setting');
        // $jsonLd[$custom_checkbox_setting] = $customCheckboxForJsonLd;    
        $jsonLd['module:customCheckbox'] = $customCheckboxForJsonLd;    
        $event->setParam('jsonLd', $jsonLd);
    }

    /*
        Renvoie CustomCheckbox pour un item    
    */
    public function getCustomCheckboxForItem($item)
    {
        if ($item instanceof ItemRepresentation) {
            $itemId = $item->id();
        } elseif ($item instanceof ItemEntity) {
            $itemId = $item->getId();
        } else {
            throw new \InvalidArgumentException('Unexpected argument type.');
        }
    
        $entityManager = $this->getServiceLocator()->get('Omeka\EntityManager');
        $dql = 'SELECT c_checkbox FROM CustomCheckbox\Entity\CustomCheckbox c_checkbox WHERE c_checkbox.item = ?1';
        $query = $entityManager->createQuery($dql)->setParameter(1, $itemId);
        return $query->getOneOrNullResult();
    }

    /*
        Hydrate CustomCheckbox pour API POST
    */
    public function hydrateCustomCheckbox(Event $event)
    {
        $itemAdapter = $event->getTarget();
        $request = $event->getParam('request');

        if (!$itemAdapter->shouldHydrate($request, 'module:customCheckbox')) {
            return;
        }

        $item = $event->getParam('entity');
        $customCheckbox = $this->getCustomCheckboxForItem($item);
        $requestCustomCheckbox = $request->getValue('module:customCheckbox', '');

        $markersAdapter = $itemAdapter->getAdapter('custom_checkbox');

        if (!$customCheckbox) {
            if ($requestCustomCheckbox === '') {
                return;
            }
            $customCheckbox = new \CustomCheckbox\Entity\CustomCheckbox;
            $customCheckbox->setItem($item);
            $this->getServiceLocator()->get('Omeka\EntityManager')->persist($customCheckbox);
        }
        
        $customCheckbox->setCustomCheckbox($requestCustomCheckbox);
    }


    /* USER CONFIG */

    /**
     * Get this module's configuration form.
     *
     * @param ViewModel $view
     * @return string
     */
    public function getConfigForm(PhpRenderer $renderer)
    {
        $formElementManager = $this->getServiceLocator()->get('FormElementManager');
        $form = $formElementManager->get(ConfigForm::class, []);
        return $renderer->formCollection($form, false);
    }

    /**
     * Handle this module's configuration form.
     *
     * @param AbstractController $controller
     * @return bool False if there was an error during handling
     */
    public function handleConfigForm(AbstractController $controller)
    {
        $params = $controller->params()->fromPost();
        if (isset($params['custom_checkbox_setting'])) {
            $custom_checkbox_setting = $params['custom_checkbox_setting'];
        }

        $globalSettings = $this->getServiceLocator()->get('Omeka\Settings');
        $globalSettings->set('custom_checkbox_setting', $custom_checkbox_setting);
    }

}

