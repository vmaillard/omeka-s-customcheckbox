<?php
namespace CustomCheckbox\Api\Representation;

use Omeka\Api\Representation\AbstractEntityRepresentation;

class CustomCheckboxRepresentation extends AbstractEntityRepresentation
{
    public function getJsonLdType()
    {
        return 'custom_checkbox';
    }

    public function getJsonLd()
    {   
        // les données exposées par la REST API sur "api/custom_checkbox"
        return [
            'o:item' => $this->item()->getReference(),
            'module:customCheckbox' => $this->custom_checkbox(),
        ];
    }

    public function item()
    {
        return $this->getAdapter('items')
            ->getRepresentation($this->resource->getItem());
    }

    public function custom_checkbox()
    {
        return $this->resource->getCustomCheckbox();
    }
}
