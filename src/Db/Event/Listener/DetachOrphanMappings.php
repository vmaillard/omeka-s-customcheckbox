<?php
namespace CustomCheckbox\Db\Event\Listener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use CustomCheckbox\Entity\CustomCheckbox;

/**
 * Automatically detach alt texts that reference unknown items.
 */
class DetachOrphanMappings
{
    /**
     * Detach all CustomCheckbox entities that reference unknown items.
     *
     * @param PreFlushEventArgs $event
     */
    public function preFlush(PreFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();
        $identityMap = $uow->getIdentityMap();

        if (isset($identityMap[CustomCheckbox::class])) {
            foreach ($identityMap[CustomCheckbox::class] as $customCheckbox) {
                if (!$em->contains($customCheckbox->getItem())) {
                    $em->detach($customCheckbox);
                }
            }
        }
    }
}
