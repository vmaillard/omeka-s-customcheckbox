<?php
namespace CustomCheckbox\Entity;

use Omeka\Entity\AbstractEntity;
use Omeka\Entity\Item;

/**
 * @Entity
 */
class CustomCheckbox extends AbstractEntity
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @OneToOne(targetEntity="Omeka\Entity\Item")
     * @JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $customCheckbox;

    public function getId()
    {
        return $this->id;
    }

    public function setItem(Item $item)
    {
        $this->item = $item;
    }

    public function getItem()
    {
        return $this->item;
    }

    public function setCustomCheckbox($customCheckbox)
    {
        $this->customCheckbox = $customCheckbox;
    }

    public function getCustomCheckbox()
    {
        return $this->customCheckbox;
    }
}
