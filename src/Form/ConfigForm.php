<?php

namespace CustomCheckbox\Form;

use Laminas\Form\Form;

class ConfigForm extends Form
{
    protected $globalSettings;
    
    public function init()
    {
        $this->add([
            'type' => 'text',
            'name' => 'custom_checkbox_setting',
            'options' => [
                'label' => 'Titre du champ',
            ],
            'attributes' => [
                'label' => 'Titre du champ',
                'value' => $this->globalSettings->get('custom_checkbox_setting'),
                'id' => 'custom_checkbox_setting',
            ],
        ]);
    }
    
    public function setGlobalSettings($globalSettings)
    {
        $this->globalSettings = $globalSettings;
    }
}